import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ShoppingBasketTest {

    private ShoppingBasket shoppingBasket;

    @BeforeEach
    public void init() {
        shoppingBasket = new ShoppingBasket();
    }

    @Test
    public void checkCorrectOperationForShoppingBasket() {

        /* test add product to basket */
        assertTrue(shoppingBasket.addToBasket("banan", 100, 1), "Dodanie produktu: banan, cena: 100, ilosc: 1");
        assertTrue(shoppingBasket.addToBasket("ser", 8500, 1), "Dodanie produktu: ser, cena: 8500, ilosc: 1");
        assertTrue(shoppingBasket.addToBasket("banan", 100, 1), "Dodanie produktu: banan, cena: 100, ilosc: 1");
        assertFalse(shoppingBasket.addToBasket("banan", 150, 1), "Dodanie produktu: banan, cena: 150, ilosc: 1");
        assertTrue(shoppingBasket.addToBasket("jablko", 300, 2), "Dodanie produktu: jablko, cena: 300, ilosc: 2");
        assertFalse(shoppingBasket.addToBasket("pomidor", 0, -1), "Dodanie produktu: pomidor, cena: 0, ilosc: -1");
        assertFalse(shoppingBasket.addToBasket("pomidor", -10, 5), "Dodanie produktu: pomidor, cena: -10, ilosc: 5");
        assertFalse(shoppingBasket.addToBasket("pomidor", 50, 0), "Dodanie produktu: pomidor, cena: 50, ilosc: 0");
        assertTrue(shoppingBasket.addToBasket("pomidor", 150, 3), "Dodanie produktu: pomidor, cena: 150, ilosc: 3");

        /* test delete product from basket */
        assertTrue(shoppingBasket.removeFromBasket("ser", 1), "Usuniecie produktu: ser, ilosc: 1");
        assertFalse(shoppingBasket.removeFromBasket("ser", 1), "Usuniecie produktu: ser, ilosc: 1");
        assertTrue(shoppingBasket.removeFromBasket("banan", 2), "Usuniecie produktu: banan, ilosc: 2");
        assertFalse(shoppingBasket.removeFromBasket("pomidor", 0), "Usuniecie produktu: pomidor, ilosc: 0");
        assertFalse(shoppingBasket.removeFromBasket("pomidor", -2), "Usuniecie produktu: pomidor, ilosc: -2");

        /* test count products */
        assertEquals(5, shoppingBasket.countProducts(), "Zliczenie ilosci produktow w koszyku");
        assertTrue(shoppingBasket.addToBasket("banan", 100, 1), "Dodanie produktu: banan, cena: 100, ilosc: 1");
        assertTrue(shoppingBasket.addToBasket("ser", 8500, 1), "Dodanie produktu: ser, cena: 8500, ilosc: 1");
        assertEquals(7, shoppingBasket.countProducts(), "Zliczenie ilosci produktow w koszyku");

        /* test get product price */
        assertEquals(100, shoppingBasket.getProductPrice("banan"), "Pobierz cene produktu: banan");
        assertEquals(8500, shoppingBasket.getProductPrice("ser"), "Pobierz cene produktu ser");
        assertEquals(-1, shoppingBasket.getProductPrice("sałata"), "Pobierz cene produktu sałata");

        /* test get total price */
        assertEquals(9650, shoppingBasket.getTotalPrice(), "Suma cen wszystkich produktow w koszyku");
        assertTrue(shoppingBasket.removeFromBasket("banan",  1), "Usuniecie produktu: banan, ilosc: 1");
        assertTrue(shoppingBasket.removeFromBasket("ser",  1), "Usuniecie produktu: ser,ilosc: 1");
        assertEquals(1050, shoppingBasket.getTotalPrice(), "Suma cen wszystkich produktow w koszyku");

    }

}
