public interface ShoppingBasketOperations {
    int ITEM_NOT_EXISTS = -1;

    // Wyswietlenie zawartosci koszyka
    String showBasketContent();

    // Zwraca true jesli item zostal dodany. Jesli item o tej nazwie i cenie istnieje to jest aktualizowana tylko jego ilosc i zwracane jest true
    // Jezeli item o cena lub ilosc jest mniejsza od 0 zwraca false
    boolean addToBasket(String itemName, int price, int quantity);

    // Jesli istnieje w koszyku taki item to jest usuwana jego odpowidnia ilosc i zwraca true
    // Jesli ilosc danego itemu koszyku po jest mniejsza lub rowna 0 to produkt jest usuwany.
    // Jesli ilosc do usuniecia jest mniejsza od 1 lub nie znaleziono itemu zwracane jest false;
    boolean removeFromBasket(String itemName, int quantity);

    // Zlicza sume wszystkich produktow w koszyku
    int countProducts();

    // Zwraca cene danego produktu jesli istnieje w koszyku, w przeciwnym wypadku zwracany jest ITEM_NOT_EXISTS (-1)
    int getProductPrice(String itemName);

    // Zwraca koszt wszystkich produktow w koszyku
    int getTotalPrice();

}
