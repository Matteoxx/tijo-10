import java.util.ArrayList;

public class ShoppingBasket implements ShoppingBasketOperations {

    private ArrayList<Item> basket = new ArrayList<>();

    @Override
    public String showBasketContent() {

        String basketContent = "";
        for(Item item : basket){
            basketContent += item.getName() + " | Cena za sztuke: " + item.getPrice() + " | Ilość : " + item.getQuantity() + "\n";
        }

        return basketContent;

    }

    @Override
    public boolean addToBasket(String itemName, int price, int quantity) {

        if(price <= 0 || quantity <= 0){
            return false;
        }

        for(Item item : basket){
            if(item.getName().toLowerCase().equals(itemName.toLowerCase()) && item.getPrice() == price) {
                item.setQuantity(item.getQuantity() + quantity);
                return true;
            } else if(item.getName().toLowerCase().equals(itemName.toLowerCase()) && item.getPrice() != price) {
                return false;
            }
        }

        Item newItem = new Item(itemName, price, quantity);
        basket.add(newItem);

        return true;

    }

    @Override
    public boolean removeFromBasket(String itemName, int quantity) {

        if(quantity < 1){
            return false;
        }

        for(Item item : basket){

            if(item.getName().toLowerCase().equals(itemName.toLowerCase())) {

                item.setQuantity(item.getQuantity() - quantity);
                if(item.getQuantity() <= 0){
                    basket.remove(item);
                }

                return true;
            }
        }

        return false;
    }

    @Override
    public int countProducts() {

        int sum = 0;
        for(Item item : basket){
            sum += item.getQuantity();
        }

        return sum;

    }

    @Override
    public int getProductPrice(String itemName) {

        int productPrice = 0;
        for(Item item : basket){
            if(item.getName().toLowerCase().equals(itemName.toLowerCase())) {
              productPrice = item.getPrice();
            }
        }

        if(productPrice != 0) {
            return productPrice;
        } else {
            return ITEM_NOT_EXISTS;
        }

    }

    @Override
    public int getTotalPrice() {

        int sum = 0;
        for(Item item : basket){
            sum += (item.getPrice() * item.getQuantity());
        }

        return sum;

    }

}
